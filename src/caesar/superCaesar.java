/**
 * Program Name:superCaesar.java
 * Purpose:
 * 
 * REVISION: THIS VERSION OF THE PROGRAM WILL PUT THE MESSAGE INTO ALL CAPS.
 * Coder: William Doyle Sec01
 * Date: Nov 16, 2018
 */
package caesar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class superCaesar
{

	public static void main(String[] args) throws FileNotFoundException																															
	{
		
		/*DECLARE VALUES EITHER FOR CLARITY OF CODE OR BECAUSE I WANT THEM IN THIS SCOPE*/
		String message = ""; 																																//will be needed no matter what and I would like to have the scope as high as possible
		String key = "";																																	// how much do we shift by when we encrypt or decrypt?
		String newStateOfMessage = "";
		
		printl("~~~Welcome to the Caesor Encryption Machine~~~ \nUsing this program you can encrypt and decrypt messages.");
		printl("\nWOULD YOU LIKE TO ENCRYPT OR DECRYPT A MESSAGE?");
		printl("1)\tENCRYPT");
		printl("2)\tDECRYPT");
		print(">>");
		String choice1 = getStringFromUser();																								//get user input (encrypt or decrypt?)
		choice1 = choice1.toUpperCase();															
		choice1 = (choice1.charAt(0) == 'E')? "1":choice1;																	//user probably wants to encrypt (THESE TWO LINES OF CODE MEAN THAT IF THE USER TYPES THE WORD 'ENCRYPT' OR THE WORD 'DECRYPT' THE PROGRAM CAN FIGURE OUT WHAT THEY WANT. THIS JUST MAKES THINGS A LITTLE MORE FLEXABLE)
		choice1 = (choice1.charAt(0)== 'D')? "2":choice1;																		//USER PROBIBLY WANTS TO DECRYPT
		if  (!(choice1.equals("1") || choice1.equals("2")))
		{
			choice1 = ensureValidValueFromUser("1", "2");
		}
		
		/*IF/ELSE STRUCTURE REFERS TO ENCRYPT AND DECRYPT CHOICE*/
		if (choice1.equals("1"))		
		{
			printl("HOW DO YOU WANT TO ENTER YOUR MESSAGE TO BE ENCRYPTED?");
			printl("1.\tI WANT TO USE THE KEYBOARD");
			printl("2.\tI WANT TO ENTER THE NAME OF A FILE CONTAINING THE MESSAGE I WANT TO ENCRYPT");
			print(">>");
			String choice2 = getStringFromUser();
			choice2 = (!(choice2.equals("1") || choice2.equals("2")))? ensureValidValueFromUser("1", "2"):choice2;		//if user gave bad input send them to a method they can't leave untill they give good input
			
			/*IF/ELSE STRUCTURE REFERS TO HOW THE USER WANTS TO ENTER THE MESSAGE*/
			if(choice2.equals("1"))//user wants to enter message via keyboard
			{
				printl("ENTER THE MESSAGE YOU WANT ENCRYPTED");
				print(">>");
				message = getStringFromUser();
			}
			else if(choice2.equals("2"))//user wants to enter location of file containing message
			{
				//get location of file holding message (String)
				printl("ENTER THE DESTINATION OF THE FILE HOLDING YOUR MESSAGE YOU WANT TO HAVE ENCRYPTED");
				print(">>");
				String nameOfFile = getStringFromUser();
				//read the String from the file
				message = readFile(nameOfFile);
				/*File file = new File(nameOfFile);//create file object using the file name entered by the user
				Scanner fileReader = new Scanner(file); //create scanner object to look through the file
				while(fileReader.hasNext())
				{
					message = message + fileReader.nextLine();//concatenate the next line of the file to the message
				}//end while
				*/
			}//end inner else (choice2)
			
			printl("ENTER A PASSWORD YOU WANT TO USE TO ENCRYPT YOUR MESSAGE");
			print(">>");
			//REVISION: HOT AND SPICY CODE HERE
			key = onlyLetters();
			int [] keyArray = new int [key.length()];
			for (int i = 0; i < keyArray.length; i++) //create 
			{
				keyArray[i] = (int)key.charAt(i) - 64; //64 because A is the 65th char in unicode and we need to subtract 1 for compensation of logic error
			}
			//END OR REVISED CODE
			
			newStateOfMessage = encrypt(message, keyArray);		//encrypts message using shiftBy value
		}//end outer if (choice1)
		else if (choice1.equals("2"))// user wants to decrypt
		{
			printl("HOW DO YOU WANT TO ENTER YOUR MESSAGE TO BE DECRYPTED?");
			printl("1.\tI WANT TO USE THE KEYBOARD");
			printl("2.\tI WANT TO ENTER THE NAME OF A FILE CONTAINING THE MESSAGE I WANT TO DECRYPT");
			print(">>");
			String choice2 = getStringFromUser();
			choice2 = (!(choice2.equals("1") || choice2.equals("2")))? ensureValidValueFromUser("1", "2"):choice2;		//if user gave bad input send them to a method they can't leave untill they give good input
			
			/*IF/ELSE STRUCTURE REFERS TO HOW THE USER WANTS TO ENTER THE JUMBLED MESSAGE*/
			if(choice2.equals("1"))//user wants to enter message via keyboard
			{
				
				printl("ENTER THE MESSAGE YOU WANT DECRYPTED");
				print(">>");
				message = getStringFromUser();
				
			}
			else if(choice2.equals("2"))//user wants to enter location of file containing JUMBLED message
			{
				
				printl("ENTER THE DESTINATION OF THE FILE HOLDING YOUR JUMBLED MESSAGE YOU WANT TO HAVE DECRYPTED");
				print(">>");
				String nameOfFile = getStringFromUser();		//get the name of the file from the user
				message = readFile(nameOfFile);			//read the String from the file
				
			}//end inner else (choice2)
			printl("ENTER THE SECRET PASSWORD TO DECRYPT YOUR MESSAGE WITH");
			print(">>");
			
			//REVISION: HOT AND SPICY CODE HERE
			key = onlyLetters();
			
			int [] keyArray = new int [key.length()];
			for (int i = 0; i < keyArray.length; i++) //create 
			{
				keyArray[i] = (int)key.charAt(i)- 64; //64 because A is the 65th char in unicode
			}
			
			//END HTO AND SPICY REVISION
			newStateOfMessage = decrypt(message, keyArray);		//encrypts message using shiftBy value
		}//end outer else
		
		
		printl("\nGIVEN MESSAGE:\t"+message);
		printl("\nSOLVED MESSAGE:\t"+newStateOfMessage);
		print("\n\n\n\n");
		printl("PLEASE ENTER THE NAME OF A FILE (ENDING WITH .txt). I WILL WRITE YOUR SOLVED MESSAGE TO THIS FILE");
		print(">>");
		String fileName = getStringFromUser();
		writeToFile(newStateOfMessage, fileName);
		print("\n\n");
		askUserIfTheyWantToRunProgramAgain();
		printl("END OF PROGRAM!");
		print("goodbye!");																																				//i like to use lowercase letters here because it looks cute
	}
	//end main
	

/**
 * METHOD NAME: readFile()
 * PURPOSE: reads a file based on a provided string file name
 * ACCEPTS: one string (the name of the file to read from)
 * RETURNS: a string (contents of that file)
 * CODER: WILLIAM DOYLE
 * DATE:  November 19th 2018
 */
	public static String readFile(String nameOfFile) throws FileNotFoundException
	{
		String message = "";
		File file = new File(nameOfFile);//create file object using the file name entered by the user
		Scanner fileReader;
		try
		{
			fileReader = new Scanner(file);
			while(fileReader.hasNext())
			{
				message = message + fileReader.nextLine();//concatinate the next line of the file to the message
			}//end while
		} catch (FileNotFoundException e)
		{
			printl("YOU GAVE A FILE THAT DOESN'T EXCIST! \nI'M GOING TO CALL MAIN() AGAIN SO YOU \nCAN RETRY THIS PROGRAM FROM THE START \nGET IT RIGHT THIS TIME!");
			main(null);				//in this method we use a try/catch statement but also throw a file not found exception. This is because in our catch statment we call main which can throw an exception itself
			print("\n\n\n\n SEE THIS NEXT PART... ITS BECAUSE YOU ENTERED A\nBAD FILE NAME AT SOME POINT AND HAD TO START OVER... YOU ARE A BAD PERSON.\n\n");
			e.printStackTrace();
		} //create scanner object to look through the file
		return message;
		
	}
	
	/**
	 * METHOD NAME: encrypt()
	 * PURPOSE: used to encrypt a message using a 'shiftBy' value to shift the letters
	 * ACCEPTS: One String and One int (String is encryptted based on int)
	 * RETURNS: One String (modified version of given string)
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static String encrypt(String message, int [] keyArray)
	{
		final byte LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'Z'; 
		final byte SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'A';
		message = message.toUpperCase(); 
		//put message into an array of ints
		int [] messageIntArray = new int[message.length()];
		int current = 0;
		for (int i = 0; i < message.length(); i++)
		{
			current = i % keyArray.length;			//tricky bit (keeps 'current' dependent on 'i' but also never lets current be so high as to make keyArray out of range
			messageIntArray[i] = (int)message.charAt(i);
			if (messageIntArray[i] >= SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR && messageIntArray[i] <= LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR)
			{
				//charecter is a lower case letter so we encrypt it
				messageIntArray[i] = messageIntArray[i] + keyArray[current];
				if (messageIntArray[i] > LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR)
				{
					//oh no! we've gone past 'z'! what do we do now? 
					//Wrap around... 
					messageIntArray[i] = keepInRange(messageIntArray[i]);
				}//end wrap e around if statement
			}//end outer if
		}//end for loop
		
		message = "";
		for (int i = 0; i < messageIntArray.length; i++)//rebuild message
		{
			message = message + (char)messageIntArray[i];
		}
		return message;
	}//end encrypt
	
	/**
	 * METHOD NAME: decrypt()
	 * PURPOSE: decrypts an encrypted message based on a shiftBy value
	 * ACCEPTS: one String (message to be decrypted) and one int (shiftBy value)
	 * RETURNS: one decrypted message
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static String decrypt(String message, int [] keyArray)
	{
		final byte LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'Z'; 
		final byte SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'A';
		message = message.toUpperCase(); 
		//put message into an array of ints
		int [] messageIntArray = new int[message.length()];
		int current = 0;
		for (int i = 0; i < message.length(); i++)
		{
			current = i % keyArray.length;			//tricky bit of code make need to be edited
			messageIntArray[i] = (int)message.charAt(i);
			if (messageIntArray[i] >= SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR && messageIntArray[i] <= LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR)
			{
				//char is a lower case letter so we encrypt it
				messageIntArray[i] = messageIntArray[i] - keyArray[current];
				if (messageIntArray[i] <  SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR)
				{
					//oh no! we've gone past 'A'! what do we do now? 
					//Wrap around... 
					messageIntArray[i] = keepInRangeDecryptVersion(messageIntArray[i]);
				}//end wrap e around if statement
			}//end outer if
		}//end for loop
		
		message = "";
		for (int i = 0; i < messageIntArray.length; i++)//rebuild message
		{
			message = message + (char)messageIntArray[i];
		}
		return message;
	}
	
	/**
	 * METHOD NAME: keepInRangeDecryptVersion()
	 * PURPOSE: uses the power of a recursive method to ensure we are within the alphabet (should be used if we go under a)
	 * ACCEPTS: an int (usually one that represents a char)
	 * RETURNS: an int (in a better form to represent a char)
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static int keepInRangeDecryptVersion(int num) 
	{
		final byte SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'A';
		final byte NUMBER_OF_LETTERS_IN_ALPHABET = 26;																				//originally just had a magic number 26 but I really don't want to lose marks
		if (num < SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR  )
		{
			num = num + NUMBER_OF_LETTERS_IN_ALPHABET; 																					// THERE ARE 26 LETTERS IN THE ALPHABET... WE NEED TO GET BACK INTO THE ALPHABET (SO JUMP BY 26 BACK INTO THE ALPHABET)
			if (num < SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR)
			{
				num = keepInRangeDecryptVersion(num);																							//call this method again (nested is beautiful)
			}
		}
		return num;
	}
	
	/**
	 * METHOD NAME: keepInRange()
	 * PURPOSE: uses the ability of a method to call itself to recursivly ensure that when we go past z we get to where we logicly should be
	 * ACCEPTS: an int (in the use case of this program the int is a value based on a charecter)
	 * RETURNS: a (probably smaller) int
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static int keepInRange(int num)
	{
		final byte LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'Z'; 
		final byte SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR = (byte)'A';
		final byte ERROR_CORRECTION_VALUE = 1;																								//used to account for a small discrepancy in the below formula
		if (num > LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR)
		{
			num = num	-  LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR + SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR - ERROR_CORRECTION_VALUE;  // note: -1 corrects logic error which only arres when we go over the last letter.. z + 1 was becoming b when it should have been a... 
			if (num > LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR )//if num is greater than upper limit we need to fix that
			{
				num = keepInRange(num);
			}//end inner if statement
		}//end outer if statement
		return num;
	}
	
	/**
	 * METHOD NAME: print()
	 * PURPOSE: prints string to standard output so I can type less code to do something so simple as write to the screen (NOTE!: NO CARAGE RETURN)
	 * ACCEPTS: String message
	 * RETURNS: NOTHING THIS IS A VOID METHOD!
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 10th 2018
	 */
	public static void print(String message)
	{
		System.out.print(message); //don't tell me what to do! I like writing methods!
	}
	
	/**
	 * METHOD NAME: printl()
	 * PURPOSE: same as print() method except this method puts a carage return charecter at the end of the line
	 * ACCEPTS: String message
	 * RETURNS: NOTHING THIS IS A VOID METHOD!
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 10th 2018
	 */
	public static void printl(String message)
	{
		System.out.println(message);
	}
	
	/**
	 * METHOD NAME: getStringFromUser()
	 * PURPOSE: gets a string from the buffer entered via keyboard (gets user input from keyboard)
	 * ACCEPTS: nothing
	 * RETURNS: One string holding the value of what the user entered
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 12th 2018
	 */
	public static String getStringFromUser() 
	{
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		return input;
	}
	
	/**
	 * METHOD NAME: ensureValidValueFromUser()
	 * PURPOSE: Once called this method will not end until the user enters a String deemed exceptable. This method should be used to quickly ensure good input!
	 * ACCEPTS: two strings. The user must enter a string that matches one of these two strings.  The two strings should be the two possible inputs the caller method wants from the user.
	 * RETURNS: a string once the user has entered the wanted information. This string is the user input
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 12th 2018
	 * NOTE: THIS IS A RECURSIVE METHOD!
	 */
	public static String ensureValidValueFromUser(String optionOne, String optionTwo)
	{
		printl("YOU HAVE ENTERED BAD INPUT! PLEASE TRY AGAIN! OPTION ONE IS "+optionOne+ " OPTION TWO IS "+optionTwo);
		print(">>");
		String input = getStringFromUser();
		if (input.equals(optionOne)||input.equals(optionTwo))
		{
			return input;
		}
		else 
		{
			printl("YOU GAVE BAD INPUT AGAIN! PLEASE GET IT RIGHT THIS TIME!");
			input = ensureValidValueFromUser(optionOne, optionTwo);
			return input;
		}
	}
	

	/**
	 * METHOD NAME: onlyLetters()
	 * PURPOSE: gets a string of only letters
	 * ACCEPTS: nothing
	 * RETURNS: a string (of only lower case letters)
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 19th 2018
	 */
	public static String onlyLetters()
	{
		final byte LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR = 90;
		final byte SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR = 65;
		
		String word = getStringFromUser();																																																							//users first attempt at entering a good value
		for (int i = 0; i < word.length(); i++)																																																					//check each charecter is valid
		{
			if (!((int)word.charAt(i)>=SMALLEST_INT_VALUE_OF_UPPER_CASE_CHAR && (int)word.charAt(i) <=  LARGEST_INT_VALUE_OF_UPPER_CASE_CHAR))					//if one of the charecters is not valid
			{
				printl("\n\nWORD ENTERED IS INVALID. YOURE STRING MUST ONLY CONTAIN LETTERS! PLEASE TRY AGAIN!\n\n");																				//inform the user that they must try again
				print(">>");
				return onlyLetters();																																																												//send the user back to the beggining of the method and return the value they give once they give a valid value
			}
		}
		return word;																																																																		//we got past the loop meaning we have a valid value... return it
	}

	/**
	 * METHOD NAME: writeToFile()
	 * PURPOSE: writes a string to a file
	 * ACCEPTS: two strings (One is the file name and the other is what we write to the file)
	 * RETURNS: nothing! this is a void method!
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 19th 2018
	 * 
	 * NOTE: try/catch is for the writer object. "throws FileNotFoundException" is for the fact that we could possibly call main (dangerous!)
	 */
	public static void writeToFile(String message, String fileName) throws FileNotFoundException
	{
		File file = new File(fileName);																														//create File object called 'file' using "fileName"
		try																																												// try and create a PrintWriter object called 'writer'
		{
			PrintWriter writer = new PrintWriter(file);
			writer.write(message);																																	//using writer object write "message" to file
			writer.close();																																					//close writer object (noting actually gets written until writer object is closed
		} //end try part of try/catch block
		catch (FileNotFoundException e)																														//the file specified by fileName is not a real file. We could panic but instead I'm going to tell the user they are an idiot and calmly restart this program
		{
			printl("BUDDY YOUR FILE DOES NOT EXCIST. \n HERE'S WHAT I'M GOING TO DO FOR YOU...\n I'M GOING TO CALL THE MAIN METHOD SO YOU CAN START AGAIN.\n TRY NOT TO SCREW IT UP THIS TIME... \n\nGOOD LUCK AND GODSPEED.");
			main(null); // give them another shot (call main method (restart program))
			//e.printStackTrace(); //commented out because its messy... left in as comment because it could be useful in the future
		}//end catch part of try/catch block (THIS IS ALSO THE END OF THE TRY/CATCH BLOCK ALL TOGETHER)
	}
	
	/**
	 * METHOD NAME: askUserIfTheyWantToRunProgramAgain()
	 * PURPOSE: asks the User If They Want To Run the Program Again
	 * ACCEPTS: nothing
	 * RETURNS: noting. This method asks for user input and based on that input either calls the main method again or does not
	 * CODER: WILLIAM DOYLEshiftBy = createShiftBy();
	 * DATE:  pre November 26th 2018
	 * @throws FileNotFoundException 
	 */
	public static void askUserIfTheyWantToRunProgramAgain() throws FileNotFoundException  //throws exception based on risks involved with calling main
	{
		printl("DO YOU WISH TO RUN THIS PROGRAM AGAIN?");																		//prompt for input
		printl("1.\tYES");
		printl("2.\tNO");
		print(">>");																																				//honestly I find the '>>' to be so clear and elegent for getting input from the user! I love doing things this way
		String answer = getStringFromUser();																								//recycle a preexcisting method to get the users response
		if (!(answer.equals("1")||(answer.equals("2"))))																		//if the user gave a response that is not what we are looking for: call method which does not allow user to leave before they give us what we want!
		{
			ensureValidValueFromUser("1", "2");
		}
		boolean goAgain =  (answer.charAt(0) == '1')? true:false;														//based on user input will we ""go again"" ---> if true call main and 'declutter the screen by placing a ton of white space on there
		if (goAgain == true)
		{
			print("CALLING main()\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");													//put some space above new instance of main so things are nice and tidy
			main(null);
		}
		else
		{
			printl("OKAY THIS IS THE END OF THE PROGRAM THEN");																//message may be redundent... consider removing
		}
	}//end askUserIfTheyWantToRunProgramAgain()
}//end class


/**
 * METHOD NAME: 
 * PURPOSE: 
 * ACCEPTS: 
 * RETURNS: 
 * CODER: WILLIAM DOYLE
 * DATE:  
 */