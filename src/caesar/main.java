/**
 * Program Name:main.java
 * Purpose: Create a caesor cyphor to encrypt and decrypt using the super caesor method
 * Coder: William Doyle Sec01
 * Date: Nov 7, 2018
 */
package caesar;

import java.io.BufferedReader;																				//used in readLineFromFile() takes a FileReader object and puts it into a buffer object. Needed in order to get contents of file
import java.io.FileNotFoundException;																	//used to satisfy java when doing things with files.  I think java wakes up in the middle of the night after having a terrable dream about not being able to find a file it was looking for. 
import java.io.FileReader;																						//used to read file (captin obvious)
import java.io.IOException;																						//used to keep java happy when it comes to buffer object. 
import java.io.PrintWriter;																						//used to write to files (pretty obvious)
import java.util.Scanner;

public abstract class main
{

	public static void main(String[] args)																															
	{
		test();																																					//USED TO TEST EVERYTHING REMOVE BEFORE HAND IN!
		printl("Welcome to the SuperCaesor Machine. Using this program you can encrypt and decrypt messages.");
		printl("Would you like to encrypt or decrypt a message?");
		printl("1)\tENCRYPT");
		printl("2)\tDECRYPT");
		print(">>");
		String choice1 = getStringFromUser();																						//does the user want to encrypt or decrypt a message?
		choice1 = choice1.toUpperCase();															
		choice1 = (choice1.charAt(0) == 'E')? "1":choice1;																	//user probibly wants to encrypt
		choice1 = (choice1.charAt(0)== 'D')? "2":choice1;																		//USER PROBIBLY WANTS TO DECRYPT
		if (choice1 != "1" && choice1 != "2")
		{
			choice1 = ensureValidValueFromUser("1", "2");
		}
		printl("NOW IN MAIN! choice1 is "+choice1);
		
		
		
	}
	
	
	/**
	 * METHOD NAME: test()
	 * PURPOSE: will be used to test code. This method will be a bunch of junk code to test methods as they are written. This method should not be included in the final project as it is known bill hates usless code.
	 * ACCEPTS: nothing
	 * RETURNS: nothing! this is a static method!
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 12th 2018
	 */
	public static void test()
	{		
		printl("------------------------START TEST METHOD-------------------------------");
		//FINALS USED TO TEST ENCRYPTION / DECRYPTION
		final String testSpaces = "      ";																//will be used to test a string of only spaces
		final String testNumbers = "187634";
		final String testAllLikeNumbers = "222222";
		final String test = "Kill a turky.";															//test a normal message
			
		String message = test;
		String key = "booleanAlgebraImeanWhatIsItMan?";
		message = encrypt(message, key);
		printl("ENCRYPTED YOUR MESSAGE LOOKS LIKE THIS:\n "+ message);		//using homemade shortcut inspired by python.
		writeToFile("hill.txt", message);
		message = decrypt(readLineFromFile("hill.txt"), key);
		print("\nDECRYPTED YOUR MESSAGE LOOKS LIKE THIS\n "+ message);
		printl("------------------------END TEST METHOD---------------------------------");
	}
	
	/**
	 * METHOD NAME: ensureValidValueFromUser()
	 * PURPOSE: Once called this method will not end until the user enters a String deemed exceptable. This method should be used to quickly ensure good input!
	 * ACCEPTS: two strings. The user must enter a string that matches one of these two strings.  The two strings should be the two possible inputs the caller method wants from the user.
	 * RETURNS: a string once the user has entered the wanted information. This string is the user input
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 12th 2018
	 * NOTE: THIS IS A RECURSIVE METHOD!
	 */
	
	public static String ensureValidValueFromUser(String optionOne, String optionTwo)
	{
		printl("YOU HAVE ENTERED BAD INPUT! PLEASE TRY AGAIN! OPTION ONE IS "+optionOne+ " OPTION TWO IS "+optionTwo);
		String input = getStringFromUser();
		if (input.equals(optionOne)||input.equals(optionTwo))
		{
			return input;
		}
		else 
		{
			printl("YOU GAVE BAD INPUT AGAIN! PLEASE GET IT RIGHT THIS TIME!");
			input = ensureValidValueFromUser(optionOne, optionTwo);
			return input;
		}
	}
	
	/**
	 * METHOD NAME: getStringFromUser()
	 * PURPOSE: gets a string from the buffer entered via keyboard (gets user input from keyboard)
	 * ACCEPTS: nothing
	 * RETURNS: One string holding the value of what the user entered
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 12th 2018
	 */
	public static String getStringFromUser() 
	{
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		//in.close();																																															//really really really wanted to be able to close the buffer but the code does not work if I close the buffer
		return input;
	}
	
	/**
	 * METHOD NAME: encrypt()
	 * PURPOSE: A Public Class Method that will encrypt a given message using a given key
	 * ACCEPTS: String message, String key
	 * RETURNS: String message (after it has been changed to be encrypted
	 * CODER: WILLIAM DOYLE
	 * DATE:  Before 10th of November 2018 (made this doc string a few days later)
	 */
	public static String encrypt(String message, String key)																
	{
		int keyLength = key.length();																				//		HOW LONG IS THE KEY STRING?	
		int [] keyArray = new int [keyLength]; 															//    CREATE AN ARRAY TO HOLD THE INT VALUES OF THE KEY 
		
		for (int i = 0; i < keyLength; i++)																	//		FILL KEYARRAY WITH INT VALUES BASED ON CHARS FROM KEY
		{
			
			keyArray[i] = (int)(key.charAt(i));													
			//System.out.println("STUB 1: " + keyArray[i]);										//		STUB
		}
		
		int messageLength = message.length();																//		HOLDS THE LENGTH OF THE MESSAGE
		char [] messageArray = new char [messageLength];										//		CREATES A CHAR ARRAY OF THAT LENGTH (WILL HOLD ALL CHARS FROM MESSAGE)
		int [] messageIntArray = new int [messageLength];										//		CREATE ANOTHER ARRAY WHICH WILL HOLD INT VALUES BASED ON THOSE CHARS
		
		for (int i = 0; i < messageLength; i++)															//		POPULATE BOTH ARRAYS WITH CHARS AND THEIR RESPECTIVE INT VALUES
		{
			messageArray[i] = message.charAt(i);															//		POPULATE WITH CHARS
			messageIntArray[i] = (int)(messageArray[i]);											//		POPULATE WITH INTS BASED ON THOSE CHARS
			
			//System.out.println("STUB 2: "+messageArray[i]);										//		STUB
		}
		
		/*
		System.out.print("STUB 3: ");																				//    STUB AND LOOP JUST SHOW WHAT THE CONTENTS OF MESSAGEINTARRAY ARE
		for (int i = 0; i < messageLength; i++)																
		{
			System.out.print(messageIntArray[i]+", ");
		}
		*/
		
		messageIntArray = rollMessage(messageIntArray, keyArray);						//		USE METHOD rollMessage() to move around the letters besed on keyArray
		
		for (int i = 0; i < messageLength; i++)															//		CONVERT THESE "SHUFFELED" INTS BACK INTO CHARS AND REPOPULATE messageArray
		{
			messageArray[i] = (char)(messageIntArray[i]);
		}
		
		message = new String(messageArray);																	//put char array back into string message
		return message;																											//return message (which is now encrypted
	}
	
	/**
	 * METHOD NAME: rollMessage()
	 * PURPOSE: "rolls values of messageIntArray to their new encrypted value" Named roll based on the cycler nature of how this is done. (cycles through the key, also cycles through askii/unicode values)
	 * ACCEPTS: int[] messageIntArray, int[] keyArray
	 * RETURNS:  rolled copy of messageIntArray
	 * CODER: WILLIAM DOYLE
	 * DATE:  Pre november 10th 2018 (Wrote doc late)
	 */
	public static int[] rollMessage(int[] messageIntArray , int [] keyArray)
	{
		for (int i = 0; i < messageIntArray.length; i++)																						//		WE WILL BE SHIFTING EVERY VALUE IN messageIntArray
		{
			for (int k = 0; k < keyArray.length; k++)																									//		HERE WE ROTATE THROUGH VALUES TO INCREMENT BY 
			{
				System.out.println("stub bro: mia[i] is "+messageIntArray[i]);
				if (messageIntArray[i] >= 97 &&  messageIntArray[i] < 122 )																											//		IF CHARECTER IS AN ACTUAL LETTER
				{
					messageIntArray[i] += keyArray[k]; 																											//		MOVE A LETTER BY A CERTIN AMOUNT
					if (k != keyArray.length-1 && !(i + 2 > messageIntArray.length))												//		WE NEED TO MANNUALLY INCREMENT 'i' THIS MAY BE CONSIDERED BAD PRACTICE BUT IF I DO THIS IN A METHOD AND GET THAT METHOD WORKING PERFECTLY THEN NOBODY WILL EVER HAVE TO WORRY ABOUT IT AGAIN. WE MUST BE CARFULL NOT TO DO AN INTEGER OVERFLOW. THIS IS WHY WE HAVE THE  && !(i + 2 > messageIntArray.length) PART
					{
						i ++;
					}
					if (messageIntArray[i] > 122)
					{
						messageIntArray[i] = subtracter(messageIntArray[i]);
					}
				}//end outer if
				if (k != keyArray.length-1 && !(i + 2 > messageIntArray.length))												//		WE NEED TO MANNUALLY INCREMENT 'i' THIS MAY BE CONSIDERED BAD PRACTICE BUT IF I DO THIS IN A METHOD AND GET THAT METHOD WORKING PERFECTLY THEN NOBODY WILL EVER HAVE TO WORRY ABOUT IT AGAIN. WE MUST BE CARFULL NOT TO DO AN INTEGER OVERFLOW. THIS IS WHY WE HAVE THE  && !(i + 2 > messageIntArray.length) PART
				{
					i ++;
				}
			}//end inner for
		}
		return messageIntArray;																																			//		RETURN OUR INT ARRAY IN GOOD FAITH THAT IT HAS BEEN PROPERLY ENCRYPTED
	}
	
	
	/**
	 * METHOD NAME: subtractor()
	 * PURPOSE: takes a number and subtracts 97 from it untill it is less than 122
	 * ACCEPTS: an int
	 * RETURNS: an int
	 * CODER: WILLIAM DOYLE
	 * DATE:  
	 */
	public static int subtracter(int num)
	{
		if (num > 122)
		{
			num = num - 97;
			num = subtracter(num);
			return num;
		}
		return num;
	}
	
	/**
	 * METHOD NAME:  decrypt()
	 * PURPOSE: to undo the encryption. To decrypt the message provided using the key provided
	 * ACCEPTS: String message (this should be an encrypted version of message), and String key
	 * RETURNS: String message (which should now be decrypted)
	 * CODER: WILLIAM DOYLE
	 * DATE:  Pre november 10th 2018 (wrote doc late)
	 */
	public static String decrypt(String message, String key)																		//		WILL TAKE THE ENCRYPTED MESSAGE AND THE KEY AND WILL RETURN THE MESSAGE SO THAT IT IS READABLE
	{
		//1) put char values of message into array of ints
		int [] messageIntArray = new int [message.length()];
		for (int i = 0; i < messageIntArray.length; i++)
		{
			messageIntArray[i] = message.charAt(i);
		}
		//2) work backwards and unroll the message do this in a method called unrollMessage()
		messageIntArray = unrollMessage( messageIntArray, key);
		
		//3) turn the array of ints back into an array of chars
		char [] messageArray = new char [messageIntArray.length];
		for (int i = 0; i < messageArray.length; i++)
		{
			messageArray[i] = (char)(messageIntArray[i]);
		}
		//4) TURN THE array of chars into a string
		message = new String(messageArray);
		//5) return decrypted message
		return message;
	}
	
	/**
	 * METHOD NAME: unrollMessage()
	 * PURPOSE: does the opposite of rollMessage(). This method is used in decryption process. it cycles backwards through the encrypted message using the key.
	 * ACCEPTS: int [] messageIntArray, String key
	 * RETURNS: messageIntArray
	 * CODER: WILLIAM DOYLE
	 * DATE:  pre november 10th 2018 (wrote doc late)
	 */
	public static int[] unrollMessage(int[] messageIntArray, String key)
	{
		int[] keyArray = new int[key.length()];
		for (int i = 0; i < keyArray.length; i++)
		{
			keyArray[i] = (int)(key.charAt(i));
		}
		
		for (int i = 0; i < messageIntArray.length; i++)																			//		NESTED LOOP IS VERY SIMILAR TO ONE THAT APPERES IN rollMessage() except it does the opposite																
		{
			for (int k = 0; k < keyArray.length; k++)																									
			{
				if (messageIntArray[i] >= 97 &&  messageIntArray[i] < 122 )																											//		IF CHARECTER IS AN ACTUAL LETTER
				{
					messageIntArray[i] -= keyArray[k]; 																											
					if (k != keyArray.length-1 && !(i + 2 > messageIntArray.length))
					{
						i ++;
					}
				}
			}
		}
		return messageIntArray;
	}
	
	/**
	 * METHOD NAME: writeToFile()
	 * PURPOSE: Will be used whenever a file is writen to
	 * ACCEPTS: String fileName, String body 
	 * RETURNS: nothing! this is a void method! 
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 10th 2018
	 */
	public static void writeToFile(String fileName, String body )
	{
		try					
		{
			PrintWriter write = new PrintWriter(fileName);																					//create PrintWriter object and call it write
			write.println(body);																																		//line actually does the writing using the object 
			write.close();																																					//closes object
		}
		catch (FileNotFoundException e)
		{
			//note: it apperes that even if exception is thrown code still does what we want it to and creates the file its looking for
			e.printStackTrace();
		}
	}
	
	
	public static String readLineFromFile(String fileName)
	{
		String line = "";
		try
		{
			FileReader file = new FileReader(fileName);
			BufferedReader buffer = new BufferedReader(file);
			line = buffer.readLine();
			buffer.close();																																					//house keeping
			file.close();																																						//eclipse didn't give me trouble for not having this line. I think buffer.close() may close the file object as well but I'd rather state it outright and clearly
		} 
		catch (FileNotFoundException e)																														// catches if something goes wrong with file object
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e)																																			//catches if something goes wrong with buffer object
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return line;
	}
	/**
	 * METHOD NAME: print()
	 * PURPOSE: prints string to standard output so I can type less code to do something so simple as write to the screen (NOTE!: NO CARAGE RETURN)
	 * ACCEPTS: String message
	 * RETURNS: NOTHING THIS IS A VOID METHOD!
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 10th 2018
	 */
	public static void print(String message)
	{
		System.out.print(message); //don't tell me what to do I like writing methods!
	}
	
	/**
	 * METHOD NAME: print()
	 * PURPOSE: same as print() mthod except this method puts a carage return charecter at the end of the line
	 * ACCEPTS: String message
	 * RETURNS: NOTHING THIS IS A VOID METHOD!
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 10th 2018
	 */
	public static void printl(String message)
	{
		System.out.println(message);
	}
}

/**
 * METHOD NAME: 
 * PURPOSE: 
 * ACCEPTS: 
 * RETURNS: 
 * CODER: WILLIAM DOYLE
 * DATE:  
 */
