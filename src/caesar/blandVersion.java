/**
 * Program Name:blandVersion.java
 * Purpose:
 * Coder: William Doyle Sec01
 * Date: Nov 16, 2018
 */
package caesar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class blandVersion
{

	public static void main(String[] args) throws FileNotFoundException																															
	{
		String message = ""; //will be needed no matter what and I would like to have the scope as high as possible
		int shiftBy = 0;		// how much do we shift by when we encrypt or decrypt?
		String newStateOfMessage = "";
		
		
		printl("Welcome to the Caesor Encryption Machine. \nUsing this program you can encrypt and decrypt messages.");
		printl("\nWould you like to encrypt or decrypt a message?");
		printl("1)\tENCRYPT");
		printl("2)\tDECRYPT");
		print(">>");
		String choice1 = getStringFromUser();																								//does the user want to encrypt or decrypt a message?
		choice1 = choice1.toUpperCase();															
		choice1 = (choice1.charAt(0) == 'E')? "1":choice1;																	//user probably wants to encrypt
		choice1 = (choice1.charAt(0)== 'D')? "2":choice1;																		//USER PROBIBLY WANTS TO DECRYPT
		if  (!(choice1.equals("1") || choice1.equals("2")))
		{
			choice1 = ensureValidValueFromUser("1", "2");
		}
		//printl("STUB: NOW IN MAIN! choice1 is "+choice1);
		
		/*IF/ELSE STRUCTURE REFERS TO ENCRYPT AND DECRYPT CHOICE*/
		if (choice1.equals("1"))		//user wants to encrypt
		{
			printl("HOW DO YOU WANT TO ENTER YOUR MESSAGE TO BE ENCRYPTED?");
			printl("1.\tI WANT TO USE THE KEYBOARD");
			printl("2.\tI WANT TO ENTER THE NAME OF A FILE CONTAINING THE MESSAGE I WANT TO ENCRYPT");
			print(">>");
			String choice2 = getStringFromUser();
			choice2 = (!(choice2.equals("1") || choice2.equals("2")))? ensureValidValueFromUser("1", "2"):choice2;		//if user gave bad input send them to a method they can't leave untill they give good input
			
			/*IF/ELSE STRUCTURE REFERS TO HOW THE USER WANTS TO ENTER THE MESSAGE*/
			if(choice2.equals("1"))//user wants to enter message via keyboard
			{
				printl("ENTER THE MESSAGE YOU WANT ENCRYPTED");
				print(">>");
				message = getStringFromUser();
			}
			else if(choice2.equals("2"))//user wants to enter location of file containing message
			{
				//get location of file holding message (String)
				printl("ENTER THE DESTINATION OF THE FILE HOLDING YOUR MESSAGE YOU WANT TO HAVE ENCRYPTED");
				print(">>");
				String nameOfFile = getStringFromUser();
				//read the String from the file
				message = readFile(nameOfFile);
				/*File file = new File(nameOfFile);//create file object using the file name entered by the user
				Scanner fileReader = new Scanner(file); //create scanner object to look through the file
				while(fileReader.hasNext())
				{
					message = message + fileReader.nextLine();//concatinate the next line of the file to the message
				}//end while
				*/
			}//end inner else (choice2)
			
			printl("ENTER A PASSWORD YOU WANT TO USE TO ENCRYPT YOUR MESSAGE");
			print(">>");
			shiftBy = createShiftBy();
			newStateOfMessage = encrypt(message, shiftBy);		//encrypts message using shiftBy value
			//printl(newStateOfMessage);
		}//end outer if (choice1)
		else if (choice1.equals("2"))// user wants to decrypt
		{
			printl("HOW DO YOU WANT TO ENTER YOUR MESSAGE TO BE DECRYPTED?");
			printl("1.\tI WANT TO USE THE KEYBOARD");
			printl("2.\tI WANT TO ENTER THE NAME OF A FILE CONTAINING THE MESSAGE I WANT TO DECRYPT");
			print(">>");
			String choice2 = getStringFromUser();
			choice2 = (!(choice2.equals("1") || choice2.equals("2")))? ensureValidValueFromUser("1", "2"):choice2;		//if user gave bad input send them to a method they can't leave untill they give good input
			
			/*IF/ELSE STRUCTURE REFERS TO HOW THE USER WANTS TO ENTER THE JUMBLED MESSAGE*/
			if(choice2.equals("1"))//user wants to enter message via keyboard
			{
				printl("ENTER THE MESSAGE YOU WANT DECRYPTED");
				print(">>");
				message = getStringFromUser();
			}
			else if(choice2.equals("2"))//user wants to enter location of file containing JUMBLED message
			{
				//get location of file holding JUMBLED message (String)
				printl("ENTER THE DESTINATION OF THE FILE HOLDING YOUR JUMBLED MESSAGE YOU WANT TO HAVE DECRYPTED");
				print(">>");
				String nameOfFile = getStringFromUser();
				//read the String from the file
				message = readFile(nameOfFile);
			}//end inner else (choice2)
			printl("ENTER THE SECRET PASSWORD TO DECRYPT YOUR MESSAGE WITH");
			print(">>");
			shiftBy = createShiftBy();
			newStateOfMessage = decrypt(message, shiftBy);		//encrypts message using shiftBy value
			//printl("MESSAGE DECRYPTED: "+ newStateOfMessage);
		}//end outer else
		
		/* heres where you need to pick up!!!
		 * 
		 *  1) write encrypt and decrypt methods (done)
		 *  1) b) reread instructions and fix the shift by issue (done)
		 *  2) create writeToFile method(done)
		 *  3) write code to give user option to write the RESULT  (newStateOfMessage) to a file. User should pick file name. (done but user has to write to file)
		 *  4) give user an option to run the program again. If they want to run it again call main() if they don't... printl("goodbye!")
		 *  
		 *  
		 *  5) finishing touch (only do this when everything else works and is backed up) add code so that if You ever get a file not found exception the user gets allerted and main() is called so they can start over
		 *  6) finally.. back up onvce again and pull out anything not used...
		 *  6) a) make it so we use capital letters instad of lowercase ones (back up everything first)
		 *  
		 *  6) b) see if theres any code in main we could give to a method (excisting or not)
		 * 
		 *  7) when this program is perfect move on the the hot and spicy version. the only major differences in that one should be in the encrypt() and decrypt() methods (though some things will need to be changed in main aswell)
		 *  */
		printl("\nGIVEN MESSAGE:\t"+message);
		printl("\nSOLVED MESSAGE:\t"+newStateOfMessage);
		print("\n\n\n\n");
		printl("PLEASE ENTER THE NAME OF A FILE (ENDING WITH .txt). I WILL WRITE YOUR SOLVED MESSAGE TO THIS FILE");
		print(">>");
		String fileName = getStringFromUser();
		writeToFile(newStateOfMessage, fileName);
		print("\n\n");
		askUserIfTheyWantToRunProgramAgain();
		printl("END OF PROGRAM!");
		print("goodbye!");
	}
	//end main
	

/**
 * METHOD NAME: readFile()
 * PURPOSE: reads a file based on a previded string file name
 * ACCEPTS: one string (the name of the file to read from)
 * RETURNS: a string (contents of that file)
 * CODER: WILLIAM DOYLE
 * DATE:  November 19th 2018
 */
	public static String readFile(String nameOfFile) throws FileNotFoundException
	{
		String message = "";
		File file = new File(nameOfFile);//create file object using the file name entered by the user
		Scanner fileReader;
		try
		{
			fileReader = new Scanner(file);
			while(fileReader.hasNext())
			{
				message = message + fileReader.nextLine();//concatinate the next line of the file to the message
			}//end while
		} catch (FileNotFoundException e)
		{
			printl("YOU GAVE A FILE THAT DOESN'T EXCIST! \nI'M GOING TO CALL MAIN() AGAIN SO YOU \nCAN RETRY THIS PROGRAM FROM THE START \nGET IT RIGHT THIS TIME!");
			main(null);
			print("\n\n\n\n SEE THIS NEXT PART... ITS BECAUSE YOU ENTERED A\nBAD FILE NAME AT SOME POINT AND HAD TO START OVER... YOU ARE A BAD PERSON.\n\n");
			e.printStackTrace();
		} //create scanner object to look through the file
		return message;
		
	}
	
	/**
	 * METHOD NAME: encrypt()
	 * PURPOSE: used to encrypt a message using a 'shiftBy' value to shift the letters
	 * ACCEPTS: One String and One int (String is encryptted based on int)
	 * RETURNS: One String (modified version of given string)
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static String encrypt(String message, int shiftBy)
	{
		final byte LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR = 122;
		final byte SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR = 97;
		message = message.toLowerCase(); 
		//put message into an array of ints
		int [] messageIntArray = new int[message.length()];
		for (int i = 0; i < message.length(); i++)
		{
			messageIntArray[i] = (int)message.charAt(i);
			if (messageIntArray[i] >= SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR && messageIntArray[i] <= LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR)
			{
				//charecter is a lower case letter so we encrypt it
				messageIntArray[i] = messageIntArray[i] + shiftBy;
				if (messageIntArray[i] > LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR)
				{
					//oh no! we've gone past 'z'! what do we do now? 
					//Wrap around... 
					messageIntArray[i] = keepInRange(messageIntArray[i]);
				}//end wrap e around if statment
			}//end outer if
		}//end for loop
		
		message = "";
		for (int i = 0; i < messageIntArray.length; i++)//rebuild message
		{
			message = message + (char)messageIntArray[i];
		}
		return message;
	}//end encrypt
	
	/**
	 * METHOD NAME: decrypt()
	 * PURPOSE: decrypts an encrypted message based on a shiftBy value
	 * ACCEPTS: one String (message to be decrypted) and one int (shiftBy value)
	 * RETURNS: one decrypted message
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static String decrypt(String message, int shiftBy)
	{
		final byte LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR = 122;
		final byte SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR = 97;
		message = message.toLowerCase(); 
		//put message into an array of ints
		int [] messageIntArray = new int[message.length()];
		for (int i = 0; i < message.length(); i++)
		{
			messageIntArray[i] = (int)message.charAt(i);
			if (messageIntArray[i] >= SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR && messageIntArray[i] <= LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR)
			{
				//charecter is a lower case letter so we encrypt it
				messageIntArray[i] = messageIntArray[i] - shiftBy;
				if (messageIntArray[i] <  SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR)
				{
					//oh no! we've gone past 'a'! what do we do now? 
					//Wrap around... 
					messageIntArray[i] = keepInRangeDecryptVersion(messageIntArray[i]);
				}//end wrap e around if statment
			}//end outer if
		}//end for loop
		
		message = "";
		for (int i = 0; i < messageIntArray.length; i++)//rebuild message
		{
			message = message + (char)messageIntArray[i];
		}
		return message;
	}
	
	/**
	 * METHOD NAME: keepInRangeDecryptVersion()
	 * PURPOSE: uses the power of a recursive method to ensure we are within the alphabet (should be used if we go under a)
	 * ACCEPTS: an int (usually one that represents a char)
	 * RETURNS: an int (in a better form to represent a char)
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 18th 2018
	 */
	public static int keepInRangeDecryptVersion(int num) 
	{
		final byte SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR = 97;
		if (num < SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR  )
		{
			num = num + 26; 																					// if we have gone to far down (below a) we need to go back up... GOTTA JUMP BACK INTO THE ALPHABET
			if (num < SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR  )
			{
				num =keepInRangeDecryptVersion(num);
			}
		}
		return num;
	}
	
	/**
	 * METHOD NAME: keepInRange()
	 * PURPOSE: uses the ability of a method to call itself to recursivly ensure that when we go past z we get to where we logicly should be
	 * ACCEPTS: an int (in the use case of this program the int is a value based on a charecter)
	 * RETURNS: a (probibly smaller) int
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 18th 2018
	 */
	public static int keepInRange(int num)
	{
		final byte LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR = 122;
		final byte SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR = 97;
		if (num > LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR )
		{
			num = num	-  LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR + SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR-1;  // note: -1 corrects logic error which only arres when we go over the last letter.. z + 1 was becoming b when it should have been a... 
			if (num > LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR )
			{
				num = keepInRange(num);
			}
		}
		return num;
	}
	
	/**
	 * METHOD NAME: print()
	 * PURPOSE: prints string to standard output so I can type less code to do something so simple as write to the screen (NOTE!: NO CARAGE RETURN)
	 * ACCEPTS: String message
	 * RETURNS: NOTHING THIS IS A VOID METHOD!
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 10th 2018
	 */
	public static void print(String message)
	{
		System.out.print(message); //don't tell me what to do I like writing methods!
	}
	
	/**
	 * METHOD NAME: printl()
	 * PURPOSE: same as print() mthod except this method puts a carage return charecter at the end of the line
	 * ACCEPTS: String message
	 * RETURNS: NOTHING THIS IS A VOID METHOD!
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 10th 2018
	 */
	public static void printl(String message)
	{
		System.out.println(message);
	}
	
	/**
	 * METHOD NAME: getStringFromUser()
	 * PURPOSE: gets a string from the buffer entered via keyboard (gets user input from keyboard)
	 * ACCEPTS: nothing
	 * RETURNS: One string holding the value of what the user entered
	 * CODER: WILLIAM DOYLE
	 * DATE:  november 12th 2018
	 */
	public static String getStringFromUser() 
	{
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		return input;
	}
	
	/**
	 * METHOD NAME: getIntFromUser()
	 * PURPOSE: gets an int from the user
	 * ACCEPTS: nothing
	 * RETURNS: an int provided by the user
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 16th 2018
	 */
	public static int getIntFromUser()
	{
		Scanner in = new Scanner(System.in);
		int input = in.nextInt();
		return input;
	}
	
	/**
	 * METHOD NAME: ensureValidValueFromUser()
	 * PURPOSE: Once called this method will not end until the user enters a String deemed exceptable. This method should be used to quickly ensure good input!
	 * ACCEPTS: two strings. The user must enter a string that matches one of these two strings.  The two strings should be the two possible inputs the caller method wants from the user.
	 * RETURNS: a string once the user has entered the wanted information. This string is the user input
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 12th 2018
	 * NOTE: THIS IS A RECURSIVE METHOD!
	 */
	public static String ensureValidValueFromUser(String optionOne, String optionTwo)
	{
		printl("YOU HAVE ENTERED BAD INPUT! PLEASE TRY AGAIN! OPTION ONE IS "+optionOne+ " OPTION TWO IS "+optionTwo);
		print(">>");
		String input = getStringFromUser();
		if (input.equals(optionOne)||input.equals(optionTwo))
		{
			return input;
		}
		else 
		{
			printl("YOU GAVE BAD INPUT AGAIN! PLEASE GET IT RIGHT THIS TIME!");
			input = ensureValidValueFromUser(optionOne, optionTwo);
			return input;
		}
	}
	
	/**
	 * METHOD NAME: createShiftBy()
	 * PURPOSE: asks user for string. uses string to generate a value from 1 to 25
	 * ACCEPTS: nothing
	 * RETURNS: an int between 1 and 25
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 19th 2018
	 */
	public static int createShiftBy()
	{
		byte LETTERS_IN_ALPHABET = 26;
		int shiftBy = 0;
		//get user input for a string
		String word = onlyLetters();
		//take String and use it to generate number
		for (int i = 0; i < word.length(); i++)
		{
			shiftBy += (int)word.charAt(i);
		}
		shiftBy = shiftBy % LETTERS_IN_ALPHABET;
		if (shiftBy == 0)
		{
			shiftBy ++;
		}
		
		return shiftBy;
	}
	
	/**
	 * METHOD NAME: onlyLetters()
	 * PURPOSE: gets a string of only letters
	 * ACCEPTS: nothing
	 * RETURNS: a string (of only lower case letters)
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 19th 2018
	 */
	public static String onlyLetters()
	{
		final byte LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR = 122;
		final byte SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR = 97;
		
		String word = getStringFromUser();																																																							//users first attempt at entering a good value
		for (int i = 0; i < word.length(); i++)																																																					//check each charecter is valid
		{
			if (!((int)word.charAt(i)>=SMALLEST_INT_VALUE_OF_A_LOWER_CASE_CHAR && (int)word.charAt(i) <=  LARGEST_INT_VALUE_OF_LOWER_CASE_CHAR))					//if one of the charecters is not valid
			{
				printl("\n\nWORD ENTERED IS INVALID. YOURE STRING MUST ONLY CONTAIN LETTERS! PLEASE TRY AGAIN!\n\n");																				//inform the user that they must try again
				print(">>");
				return onlyLetters();																																																												//send the user back to the beggining of the method and return the value they give once they give a valid value
			}
		}
		return word;																																																																		//we got past the loop meaning we have a valid value... return it
	}

	/**
	 * METHOD NAME: writeToFile()
	 * PURPOSE: writes a string to a file
	 * ACCEPTS: two strings (One is the file name and the other is what we write to the file)
	 * RETURNS: nothing! this is a void method!
	 * CODER: WILLIAM DOYLE
	 * DATE:  November 19th 2018
	 * 
	 * NOTE: we use a try/catch to call main if anything goes wrong but we need to also have a throws statment at the top of this method to handle any issues main() could give us
	 */
	public static void writeToFile(String message, String fileName) throws FileNotFoundException
	{
		File file = new File(fileName);
		try
		{
			PrintWriter writer = new PrintWriter(file);
			writer.write(message);
			writer.close();
		} 
		catch (FileNotFoundException e)
		{
			printl("BUDDY YOUR FILE DOES NOT EXCIST. \n HERE'S WHAT I'M GOING TO DO FOR YOU...\n I'M GOING TO CALL THE MAIN METHOD SO YOU CAN START AGAIN.\n TRY NOT TO SCREW IT UP THIS TIME... \n\nGOOD LUCK AND GODSPEED.");
			main(null); // give them another shot
			//e.printStackTrace();
		}
	}
	
	/**
	 * METHOD NAME: 
	 * PURPOSE: 
	 * ACCEPTS: 
	 * RETURNS: 
	 * CODER: WILLIAM DOYLE
	 * DATE:  
	 * @throws FileNotFoundException 
	 */
	public static void askUserIfTheyWantToRunProgramAgain() throws FileNotFoundException
	{
		printl("DO YOU WISH TO RUN THIS PROGRAM AGAIN?");
		printl("1.\tYES");
		printl("2.\tNO");
		print(">>");
		String answer = getStringFromUser();
		if (!(answer.equals("1")||(answer.equals("2"))))
		{
			ensureValidValueFromUser("1", "2");
		}
		boolean goAgain;
		goAgain =  (answer.charAt(0) == '1')? true:false;
		if (goAgain == true)
		{
			print("CALLING main()\n\n\n\n\n\n\n\n\n\n\n\n\n");
			main(null);
		}
		else
		{
			printl("OKAY THIS IS THE END OF THE PROGRAM THEN");
		}
				
		
	}
	

}
//end class


/**
 * METHOD NAME: 
 * PURPOSE: 
 * ACCEPTS: 
 * RETURNS: 
 * CODER: WILLIAM DOYLE
 * DATE:  
 */